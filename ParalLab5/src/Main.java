import java.util.HashSet;
import java.util.Scanner;

public class Main {

    public static void search(int [][] matrix){
        Scanner sc = new Scanner(System.in);
        System.out.println("Your number:");
        int num = sc.nextInt();
        HashSet<Integer> result = new HashSet<>();
        // omp parallel for
        for(int i = 0;i<matrix.length;i++) {
            // omp parallel for
            for (int j = 0; j < matrix[0].length; j++) {
                if (matrix[i][j] == num)
                    result.add(i + 1);
            }
        }
        System.out.println("Result: " + result.toString());
    }

    public static int[][] input(){
        int [][] matrix;
        System.out.println("Incidence matrix\n0 - demo\nelse - your data");
        Scanner sc = new Scanner(System.in);
        int in = sc.nextInt();
        if(in == 0){
            matrix  = new int[][]{{-1, 1, -1, 0, -1, 0}, {1, 0, 0, -1, 0, 0}, {0, -1, 0, 0, 0, 1},
                    {0, 0, 1, 0, 0, -1}, {0, 0, 0, 1, 1, 0}};
        }
        else {
            System.out.println("Number of vertices:");
            int vertNum = sc.nextInt();
            System.out.println("Number of edges:");
            int edgenum = sc.nextInt();
            System.out.println("Matrix:");
            matrix = new int[vertNum][edgenum];
            for(int i = 0;i<vertNum;i++)
                for (int j = 0;j<edgenum;j++)
                    matrix[i][j] = sc.nextInt();
        }
        return matrix;
    }

    public static void main(String[] args) {
        int [][] matrix = input();
        search(matrix);
    }
}
