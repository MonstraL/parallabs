

import java.util.Arrays;
import java.util.Random;
import java.util.function.IntBinaryOperator;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class LeonardoHeapOfInt {
    private final static int[] LP = Stream
            .iterate(new long[]{1, 1}, s -> new long[]{s[1], s[0] + s[1] + 1L})
            .mapToLong(n -> n[0]).takeWhile(lll -> lll < (long) Integer.MAX_VALUE)
            .mapToInt(lll -> (int) lll).toArray();

   
    private long s;
    private int t;

    // the actual sub-array to be implicitly reconfigured
    // into a Leonardo heap is m[left, right]



    private final int[] m;
    // the index of the first element, inclusive, to be sorted
    final int left;
    // the index of the last element, inclusive, to be sorted
    int right;
    // the comparator that specifies the order within the heap
    private IntBinaryOperator cmp;

    public LeonardoHeapOfInt(int[] a, int fromIndex, int toIndex, IntBinaryOperator cmp) {
        rangeCheck(a.length, fromIndex, toIndex);
        this.m = a;
        this.left = fromIndex;
        this.cmp = cmp;
        heapify(toIndex);
    }

    static void rangeCheck(int arrayLength, int fromIndex, int toIndex) {
        if (fromIndex > toIndex) {
            throw new IllegalArgumentException("fromIndex(" + fromIndex + ") > toIndex(" + toIndex + ")");
        }
        if (fromIndex < 0) {
            throw new ArrayIndexOutOfBoundsException(fromIndex);
        }
        if (toIndex > arrayLength) {
            throw new ArrayIndexOutOfBoundsException(toIndex);
        }
    }
    private void heapify(int toIndex) {
        this.right = toIndex-1;
        if (left == right) {
            this.s = 0b0;
            this.t = 0;
        } else if (left+1 == right) {

            this.s = 0b0;
            this.t = 1;
        } else {
            this.s = 0b1;
            this.t = 0;
	// omp sections
	{
	// omp section
	{
            for (int i = left+1; i < right; ++i) {
            
                if ((s & 0b1) == 0b1) {
		 // omp sections
	{
	// omp section
	{
                    sift(m, i, t, cmp);
	}
// omp section
	{
                    assert (checkHeapProperty(m, i, s, t, cmp));
}
// omp section
	{
                    s >>>= 2;
}
// omp section
	{
                    t = t + 2;
}
}

                } else {
                    assert (t > 0);
                    final int c = LP[t - 1];
                    if (i + c < right) {   
                        sift(m, i, t, cmp);
                        assert (checkHeapProperty(m, i, s, t, cmp));
                    } else { 
                        trinkle(m, i, s, t, cmp);
                        assert (checkOrderProperty(m, i, s, t, cmp));
                    }

                    if (t != 1) {
                        s = ((s << 1) | 0b1) << (t - 2);
                        t = 1;
                    } else {
                        t = 0;
                        s = (s << 1) | 0b1;
                    }
                }
            }
	}
	// omp section
		{

            trinkle(m, right, s, t, cmp);

            assert (checkHeapProperty(m, right, s, t, cmp));
            assert (checkOrderProperty(m, right, s, t, cmp));
		}
           }
        }
    }

    
    private static void sift(final int[] m, int i, int t, IntBinaryOperator cmp) {
        // preserve the original root value
        final int root = m[i];
        while (t > 1) {
            final int j = i - LP[t - 2] - 1;
            final int k = i - 1;

            assert (j >= 0 && k >= 0);

            if (cmp.applyAsInt(root, m[j]) >= 0 && cmp.applyAsInt(root, m[k]) >= 0) {
        break;
            }

            if (cmp.applyAsInt(m[j], m[k]) >= 0) {

                m[i] = m[j];

                i = j;
                t = t - 1;
            } else {

                m[i] = m[k];
                i = k;
                t = t - 2;
            }
        }
        // finally swap the original root into the position of the last stretch
        m[i] = root;
    }

    
    private static void trinkle(final int[] m, int i, long s, int t, IntBinaryOperator cmp) {
        // preserve the original root value
        final int root = m[i];

        for(int trail; s!=0; trail = Long.numberOfTrailingZeros(s) + 1, s >>>= trail, t += trail) {
            
            int i3 = i - LP[t];
            // regard m[i3] as the third side subtree (stepchild) of the stretch to its right
            if (cmp.applyAsInt(root, m[i3]) >= 0) {
                // root does not need to trinkled further
                break;
            }
           
            if (t > 1) {
                final int k = i - 1;
                if (cmp.applyAsInt(m[k], m[i3]) >= 0) {
                    break;
                }
                final int j = i - LP[t - 2] - 1;
                if (cmp.applyAsInt(m[j], m[i3]) >= 0) {
                    break;
                }
            }
            m[i] = m[i3];
            i = i3;
        }
        m[i] = root;
        sift(m, i, t, cmp);
    }

    private static boolean checkHeapProperty(int[] m, int i, long s, int t, IntBinaryOperator cmp) {
        for(int trail; s!=0; i -= LP[t], trail = Long.numberOfTrailingZeros(s) + 1, s >>>= trail, t += trail) {
            if (!checkHeapProperty(m, i, t, cmp)) {
                return false;
            }
        }
        return true;
    }

   
    private static boolean checkHeapProperty(int[] m, int i, int t, IntBinaryOperator cmp) {
        if (t <= 1) return true;
        if (t == 2) {
            return cmp.applyAsInt(m[i], m[i - 1]) >= 0;
        }
	

        final int root = m[i];
        // the left subtree, LP[t−1], is rooted at
        final int j = i - LP[t - 2] - 1;
        // the right subtree, LP[t−2], is rooted at
        final int k = i - 1;
        if (cmp.applyAsInt(root, m[j]) >= 0 && cmp.applyAsInt(root, m[k]) >= 0 && checkHeapProperty(m, j, t - 1, cmp) && checkHeapProperty(m, k, t - 2, cmp)) {
            return true;
        }
        assert false : String.format("FAILED: Heap-property for tree LP[%d] rooted at i=%d since the root of either of its subtrees is more extreme!", t, i);
        return false;
    }

    
    private static boolean checkOrderProperty(int[] m, int i, long s, int t, IntBinaryOperator cmp) {
        int rightMoreExtreme = m[i];
        for (int trail; s != 0; i -= LP[t], trail = Long.numberOfTrailingZeros(s) + 1, s >>>= trail, t += trail) {
            if (cmp.applyAsInt(rightMoreExtreme, m[i]) < 0) {
                assert false : String.format("FAILED: Order-property for tree LP[%d] rooted at i=%d is more extreme than its left neighbor!", t, i);
                return false;
            }
            rightMoreExtreme = m[i];
        }
        return true;
    }


    public boolean isEmpty() {
        return this.s == 0b0 && this.t <= 0;
    }

    
    private static void swap(final int[] arr, final int lhs, final int rhs) {
        final int temp = arr[lhs];
        arr[lhs] = arr[rhs];
        arr[rhs] = temp;
    }

    
    private void pop() {
        if(isEmpty()) {

            return;
        }
        if (t == 0) {
            assert (s & 0b1) == 0b1 : String.format("ERROR: Since right≥2, LP[0] cannot be present without LP[1] being present too ");
            assert (checkHeapProperty(m,right,s,t,cmp));
            assert (checkOrderProperty(m, right, s, t, cmp));


            s >>>= 1;
            t = 1;
        } else if (t == 1) {
            assert (checkHeapProperty(m,right,s,t,cmp));
            assert (checkOrderProperty(m, right, s, t, cmp));


            final int trail = (s!=0b0) ? Long.numberOfTrailingZeros(s) + 1 : -1;
            s >>>= trail;
            t += trail;
        } else {
            // if this point is reached, the rightmost tree, LP[t],
            // is decomposed into LP[t-1] and LP[t-2].

            // the new second-to-rightmost tree, LP[t−1], is rooted at
            final int j = right - LP[t - 2] - 1;
            // the new rightmost tree, LP[t−2], is rooted at
            final int k = right - 1;

            if (s != 0) {

                final int i3 = right - LP[t];

                if (cmp.applyAsInt(m[i3], m[j]) > 0) {

                    swap(m, j, i3);

                    final int trail = Long.numberOfTrailingZeros(s) + 1;
                    trinkle(m, i3, s >>> trail, t + trail, cmp);
                }

            }

            if (cmp.applyAsInt(m[j], m[k]) > 0) {

                swap(m, k, j);

                trinkle(m, j, s << 1, t - 1, cmp);

            }

            assert (checkHeapProperty(m,right,s,t,cmp));
            assert (checkOrderProperty(m, right, s, t, cmp));

            s = (s << 2) | 0b1;
            t = t - 2;
        }

        --right;
    }

    
    public static void smoothsort(int[] a, int fromIndex, int toIndex, IntBinaryOperator cmp) {
        if(fromIndex == toIndex) {
            // the sub-array to sort is empty, so nothing to do
            return;
        }
        LeonardoHeapOfInt lheap = new LeonardoHeapOfInt(a, fromIndex, toIndex, cmp);
        for(int i=toIndex; --i>=fromIndex; ) {
   
            lheap.pop();
        }
    }

    public static void main(String[] args) {
	    int [] mass = {2,1,16,4,8,5,6,9,15,11,20,19};
        System.out.println("Before sorting:");
        for(int i = 0;i<mass.length;i++)
            System.out.println(mass[i]);
        LeonardoHeapOfInt.smoothsort(mass, 0, mass.length, (int lhs, int rhs) -> (lhs - rhs));
        System.out.println("After sorting:");
	for(int i = 0;i<mass.length;i++)
        	System.out.println(mass[i]);
    }
}
