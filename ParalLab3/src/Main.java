public class Main {

    public static void main(String[] args) {
	    int n = 11;
	    double p = 1;
        // omp parallel for
	    for(int k = 1;k<n;k++){
	        p*=Math.sin((double)k)/(double)k;
            System.out.println(p);
        }
    }
}
