import java.math.BigInteger;

public class Main {
    public static void power(int num, int deg){
        BigInteger result = new BigInteger(String.valueOf(num));
        for(int i = 1;i<deg;i++) {
            result = result.multiply(BigInteger.valueOf(num));
        }
        System.out.println("Result: " + result);
    }

    public static void main(String[] args) {
        int num = 2, deg = 111;
        // omp parallel
		{
			// omp critical
			{
            System.out.println("Лабораторна робота №1. Знайомство з OpenMP");
            System.out.println("Обчислюємо ступінь двійки ( " + deg + " ):");
            power(num, deg);
      			 }
		}
    }
}
