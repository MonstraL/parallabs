import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void bubSort(int [] A){
        for(int i = 0;i<A.length-1;i++){
            if(i%2==0){
                // omp parallel for
                for(int j = 0;j<A.length/2;j++){
                    if(A[2*j]>A[2*j+1])
                    {
                        int temp = A[2*j];
                        A[2*j] = A[2*j+1];
                        A[2*j+1] = temp;
                    }
                }
            }
            else{
                // omp parallel for
                for(int j = 0;j<A.length/2-1;j++){
                    if(A[2*j+1]>A[2*j+2])
                    {
                        int temp = A[2*j+1];
                        A[2*j+1] = A[2*j+2];
                        A[2*j+2] = temp;
                    }
                }
            }
        }
    }

    public static int [] toDoubleArray(int [][] num){

        int[] flat = new int[num.length * num[0].length];

        int ctr = 0;
        for (int row = 0; row < num.length; row++) {
            for (int col = 0; col < num[0].length; col++) {
                flat[ctr++] = num[row][col];
            }
        }
        return flat;
    }

    public static int [][] toSimpleArray(int [] flat, int r, int c){
        int ctr = 0;
        int [][] num = new int[r][c];
        for (int row = 0; row < r; row++) {
            for (int col = 0; col < c; col++) {
                num[row][col] = flat[ctr++];
            }
        }
        return num;
    }

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter a number of rows: ");
        int row = reader.nextInt();
        System.out.println("Enter a number of cols: ");
        int col = reader.nextInt();
        int [][] massD = new int[row][col];
        Random r = new Random();
        // omp parallel for
        for (int i = 0; i < massD.length; i++) {
            // omp parallel for
            for(int j = 0;j<massD[1].length;j++) {
                massD[i][j] = r.nextInt();
            }
        }
        int [] mass = toDoubleArray(massD);
        long startTime = System.nanoTime();
        bubSort(mass);
        long endTime   = System.nanoTime();
        for(int i = 0;i<mass.length;i++)
            System.out.println(mass[i]);
        massD = toSimpleArray(mass,massD.length,massD[1].length);
        System.out.println("Sorting time of " + mass.length + " numbers = " + (endTime-startTime) + "ns");
    }
}
