public class Main {

    static final int N = 6000;

    public static int [][] sum(int [][] firstM, int [][] secondM) {
        int [][] resultM = secondM;
        // omp parallel for
        for(int i = 0;i<N;i++) {
            // omp parallel for
            for(int j = 0;j<N;j++) {
                resultM[i][j] = firstM[i][j] + secondM[i][j];
            }
        }
        return resultM;
    }

    public static void main(String[] args) {
        int[][] firstM = new int[N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                firstM[i][j] = i + j;
        int[][] secondM = firstM;
        int[][] resultM = sum(firstM, secondM);
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++)
                System.out.print(resultM[i][j] + " ");
            System.out.println();
        }
    }
}
